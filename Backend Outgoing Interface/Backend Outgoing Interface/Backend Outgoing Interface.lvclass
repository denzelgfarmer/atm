﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Backend Outgoing Interface.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Backend Outgoing Interface.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.IsInterface" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="balance response.vi" Type="VI" URL="../balance response.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;(!!!!"1!%!!!!&lt;E"Q!"Y!!%5A1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:M;7)C1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:D&lt;'&amp;T=Q!?1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5A&lt;X6U!!!.1!I!"U*B&lt;'&amp;O9W5!&lt;%"Q!"Y!!%5A1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:M;7)C1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:D&lt;'&amp;T=Q!&gt;1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5A;7Y!6!$Q!!Q!!!!!!!!!!1!!!!!!!!!!!!!!!A!!!!-#!!"Y!!!!!!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!*!!!!!!!1!%!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="deposit response.vi" Type="VI" URL="../deposit response.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!"1!%!!!!&lt;E"Q!"Y!!%5A1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:M;7)C1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:D&lt;'&amp;T=Q!?1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5A&lt;X6U!!!81!I!%%&amp;N&lt;X6O&gt;#"%:8"P=WFU:71!!'R!=!!?!!"&amp;)%*B9WNF&lt;G1A4X6U:W^J&lt;G=A37ZU:8*G97.F,GRW&lt;'FC)E*B9WNF&lt;G1A4X6U:W^J&lt;G=A37ZU:8*G97.F,GRW9WRB=X-!(5*B9WNF&lt;G1A4X6U:W^J&lt;G=A37ZU:8*G97.F)'FO!&amp;1!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!)!!!!$!A!!?!!!!!!!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!!!!!!!#1!!!!!!%!"!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="login response.vi" Type="VI" URL="../login response.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;C!!!!"Q!%!!!!&lt;E"Q!"Y!!%5A1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:M;7)C1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:D&lt;'&amp;T=Q!?1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5A&lt;X6U!!!/1$$`````"%RB=X1!!!Z!-0````]&amp;2GFS=X1!$%!B"E6Y;8.U=Q!!&lt;%"Q!"Y!!%5A1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:M;7)C1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:D&lt;'&amp;T=Q!&gt;1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5A;7Y!6!$Q!!Q!!!!!!!!!!1!!!!!!!!!!!!)!!Q!%!!5#!!"Y!!!!!!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!B!!!!)1!!!!%!!!!*!!!!!!!1!'!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685376</Property>
	</Item>
	<Item Name="withdraw response.vi" Type="VI" URL="../withdraw response.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&gt;!!!!"A!%!!!!&lt;E"Q!"Y!!%5A1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:M;7)C1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5O&lt;(:D&lt;'&amp;T=Q!?1G&amp;D;W6O:#"0&gt;82H&lt;WFO:S"*&lt;H2F=G:B9W5A&lt;X6U!!!81!I!%%&amp;N&lt;X6O&gt;#"8;82I:(*B&gt;WY!!!R!)1:'97FM:71!!'R!=!!?!!"&amp;)%*B9WNF&lt;G1A4X6U:W^J&lt;G=A37ZU:8*G97.F,GRW&lt;'FC)E*B9WNF&lt;G1A4X6U:W^J&lt;G=A37ZU:8*G97.F,GRW9WRB=X-!(5*B9WNF&lt;G1A4X6U:W^J&lt;G=A37ZU:8*G97.F)'FO!&amp;1!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!)!!Q!%!A!!?!!!!!!!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!!"!!!!#1!!!!!!%!"1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
</LVClass>
