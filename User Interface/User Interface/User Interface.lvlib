﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Messages for this Actor" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="account login response Msg.lvclass" Type="LVClass" URL="../User Interface Messages/account login response Msg/account login response Msg.lvclass"/>
		<Item Name="balance response Msg.lvclass" Type="LVClass" URL="../User Interface Messages/balance response Msg/balance response Msg.lvclass"/>
		<Item Name="withdraw response Msg.lvclass" Type="LVClass" URL="../User Interface Messages/withdraw response Msg/withdraw response Msg.lvclass"/>
		<Item Name="deposit response Msg.lvclass" Type="LVClass" URL="../User Interface Messages/deposit response Msg/deposit response Msg.lvclass"/>
		<Item Name="awaiting backend Msg.lvclass" Type="LVClass" URL="../User Interface Messages/awaiting backend Msg/awaiting backend Msg.lvclass"/>
		<Item Name="button pressed Msg.lvclass" Type="LVClass" URL="../User Interface Messages/button pressed Msg/button pressed Msg.lvclass"/>
		<Item Name="main menu state Msg.lvclass" Type="LVClass" URL="../User Interface Messages/main menu state Msg/main menu state Msg.lvclass"/>
		<Item Name="welcome state Msg.lvclass" Type="LVClass" URL="../User Interface Messages/welcome state Msg/welcome state Msg.lvclass"/>
		<Item Name="helper loop error Msg.lvclass" Type="LVClass" URL="../User Interface Messages/helper loop error Msg/helper loop error Msg.lvclass"/>
	</Item>
	<Item Name="buttons.ctl" Type="VI" URL="../buttons.ctl"/>
	<Item Name="UI State.ctl" Type="VI" URL="../UI State.ctl"/>
	<Item Name="User Interface.lvclass" Type="LVClass" URL="../User Interface/User Interface.lvclass"/>
</Library>
